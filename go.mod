module codeberg.org/gerald/feed-update-check

go 1.22.3

require (
	codeberg.org/gerald/signal-cli-client v0.1.1
	github.com/SlyMarbo/rss v1.0.6-0.20240421150216-4c0036e80867
)

require github.com/axgle/mahonia v0.0.0-20180208002826-3358181d7394 // indirect
