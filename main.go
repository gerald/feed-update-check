package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/SlyMarbo/rss"

	signal "codeberg.org/gerald/signal-cli-client"
	signalflags "codeberg.org/gerald/signal-cli-client/flags"
)

var (
	flagFeedURL, flagLaterThan, flagSocksProxy string
	flagParsedLaterThan                        time.Duration
)

func validateFlags() {
	flagsAreValid := true

	if flagFeedURL == "" {
		log.Println("flag -u requires a value")
		flagsAreValid = false
	} else {
		u, err := url.Parse(flagFeedURL)
		if err != nil || u.Scheme == "" || u.Host == "" {
			log.Println("flag -u value is not a valid URL.")
			flagsAreValid = false
		}
	}

	if flagLaterThan == "" {
		log.Println("flag -l requires a value")
		flagsAreValid = false
	} else {
		duration, err := time.ParseDuration(flagLaterThan)
		if err != nil {
			log.Printf("flag -l value is not a valid duration. %v\n", err)
			flagsAreValid = false
		}

		flagParsedLaterThan = duration
	}

	if flagSocksProxy != "" {
		u, err := url.Parse("socks5://" + flagSocksProxy)
		if err != nil || u.Host == "" {
			log.Println("flag -proxy value is not a valid host:port combination.")
			flagsAreValid = false
		}
		log.Printf("Using 'socks5://%s' as proxy", flagSocksProxy)
	}

	if flagsAreValid == false {
		os.Exit(1)
	}
}

func fetchFeed() (*rss.Feed, error) {
	if flagSocksProxy == "" {
		return rss.Fetch(flagFeedURL)
	}
	proxyURL, err := url.Parse("socks5://" + flagSocksProxy)
	if err != nil {
		return nil, fmt.Errorf("proxy setup: cannot parse proxy url 'socks5://%s': %w", flagSocksProxy, err)
	}

	transport := http.Transport{}
	transport.Proxy = http.ProxyURL(proxyURL)
	// // transport.TLSClientConfig = &tls.Config{InsecureSkipVerify: true} //set ssl
	// client :=

	return rss.FetchByClient(flagFeedURL, &http.Client{Transport: &transport})
}

func main() {
	flag.StringVar(&flagFeedURL, "u", "", "feed url.")
	flag.StringVar(&flagLaterThan, "l", "", "only show items within the given duration. specify as golang duration string (example: 1h10m10s)")
	flag.StringVar(&flagSocksProxy, "proxy", "", "socks5 proxy host:port, no proxy will be used if empty")
	flag.Parse()
	// validate flags
	validateFlags()

	signalConfig, err := signalflags.Get()
	if err != nil {
		log.Fatal(err)
	}

	comparisonDate := time.Now().Add(-flagParsedLaterThan)

	feed, err := fetchFeed()
	if err != nil {
		log.Fatalf("Error fetching feed: %v\n", err)
	}

	var b strings.Builder

	// only print items newer
	for _, item := range feed.Items {
		if item.Date.After(comparisonDate) {
			fmt.Fprintf(&b, "[%s]: %s: %s\n", item.Date.Format(time.DateTime), item.Title, item.ID)
		}
	}

	notificationStr := b.String()

	if notificationStr != "" {
		c, err := signal.NewClient(signalConfig.APIEndpoint, signalConfig.Sender)
		if err != nil {
			log.Fatal(err)
		}

		err = c.SendMessage(signalConfig.Remotes, notificationStr)
		if err != nil {
			log.Fatalf("Error fetching feed: %v\n", err)
		}
	}

}
