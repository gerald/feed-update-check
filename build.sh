#!/bin/bash

BIN=feed-update-check

FLAGS=-ldflags="-s -w"

GOAMD64=v2 CGO_ENABLED=0 go build -o "$BIN" "$FLAGS" main.go

#CGO_ENABLED=0 GOOS=linux GOARCH=arm GOARM=6 go build -o "$BIN"-arm6 "$FLAGS" main.go
