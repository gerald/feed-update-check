# feed-update-check

Cli for checking new items of a RSS feed.

Inspired by [rsstail](https://github.com/flok99/rsstail)

## License

MIT
